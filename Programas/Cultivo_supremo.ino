int potPin = A0;  // Pin analógico utilizado para leer el valor del potenciómetro

void setup() {
  Serial.begin(9600);  // Inicializar la comunicación serial a 9600 bps
}

void loop() {
  // Leer el valor del potenciómetro
  int potValue = analogRead(potPin);
  
  // Mapear el valor del potenciómetro al rango de salida del PLC (por ejemplo, de 0 a 100)
  int plcOutput = map(potValue, 0, 1023, 0, 100);
  
  // Enviar el valor por la comunicación serial a Node-RED
  Serial.println(plcOutput);
  
  // Enviar el valor al PLC S7-1200 a través de la comunicación serial
  // Aquí deberías agregar el código o protocolo de comunicación adecuado para enviar el valor al PLC
  
  delay(500);  // Pequeña pausa antes de enviar el siguiente valor
}
